<!DOCTYPE html>
<html>
  <head>
    <title>Final Project</title>
    <cfinclude template = "./css/style.css">
  </head>
  <body>

    <cfparam name="projectName" default="Final Project" type="string">
    <cfparam name="URL.candidateid" default="AAA" type="string">
    <cfinclude template = "header.cfm">

    <!-- if true, no campaigns have been provided in the URL, so ALL campaigns will be displayed -->
    <cfif URL.candidateid EQ "AAA">  
   
    <cfquery name="getCampaigns"
             datasource="#Request.DSN#"
             username="#Request.username#"
             password="#Request.password#">
      SELECT campaignid, can.candidateid, candidatename, o.description as office, a.description area, term 
      FROM tbcandidate can, tboffice o, tbarea a, tbcampaign cam
      WHERE can.candidateid = cam.candidateid
      AND   o.officeid = cam.officeid
      AND   a.areaid = cam.areaid
      ORDER BY 3
    </cfquery>

    <h4>Campaigns</h4>
    <table>
         <tr><th>ID</th><th>Candidate</th><th>Running For</th><th>Area</th><th>Term</th></tr>
        <cfoutput query="getCampaigns">
         <tr>
          <td>#campaignid#</td>
          <!-- the href below will forward to a page displaying only this candidates campaigns -->
          <td><a href="campaigns.cfm?candidateid=#candidateid#" title="See campaigns run by this candidate">#candidatename#</a></td>
          <td>#office#</td>
          <td>#area#</td>
          <td>#term#</td>
         </tr>
        </cfoutput>
    </table>


  <!-- if here, a candidate has been selected and only his campains should be shown -->
  <cfelse>

    <!-- if true, the campaign will be deleted -->
    <cfif isdefined("Form.delete")>
      <cfquery  name="deleteCampaign"
              datasource="#Request.DSN#"
              username="#Request.username#"
              password="#Request.password#">
       DELETE FROM tbcampaign
       WHERE campaignid = 
          <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#Form.campaignid#">
    </cfquery>
    <cfoutput><cflocation url="campaigns.cfm?candidateid=#candidateid#"></cfoutput>

    
    <cfelse> <!-- not a delete, so show campaigns from a given candidate -->

    <cfquery  name="getCandidate"
              datasource="#Request.DSN#"
              username="#Request.username#"
              password="#Request.password#">
       SELECT candidateid FROM tbcandidate
       WHERE candidateid = 
          <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#URL.candidateid#">
    </cfquery>

      <cfif getCandidate.RecordCount IS 0>
        Candidate not in database. <br>
        Please <a href="candidates.cfm">try again</a>

      <cfelse> <!-- there are candidates in the database -->

      <cfquery name="getCampaigns2"
               datasource="#Request.DSN#"
               username="#Request.username#"
               password="#Request.password#">
        SELECT campaignid, candidatename, o.description as office, a.description area, term 
        FROM tbcandidate can, tboffice o, tbarea a, tbcampaign cam
        WHERE can.candidateid = cam.candidateid
        AND   o.officeid = cam.officeid
        AND   a.areaid = cam.areaid
        AND   cam.candidateid =
        <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#URL.candidateid#">
      </cfquery>


      <h4>Campaigns</h4>
      <table> <!-- display all campaings by given candidate -->
           <tr><th>ID</th><th>Candidate</th><th>Running For</th><th>Area</th><th>Term</th></tr>
          <cfoutput query="getCampaigns2">
           <tr>
            <td>#campaignid#</td>
            <td><a href="campaigns.cfm?candidateid=#candidateid#" title="See campaigns run by this candidate">#candidatename#</a></td>
            <td>#office#</td>
            <td>#area#</td>
            <td>#term#</td>
            <td>
              <cfform> <!-- passing these values for delete in case the delete button is pressed -->
              <input name="campaignid" type="hidden" value="#campaignid#">
              <input name="candidateid" type="hidden" value="#candidateid#">
              <input name="delete" type="submit" value="Delete">
            </cfform>
            </td>
           </tr>
          </cfoutput>
      </table>

      </cfif>
      </cfif>
  </cfif>

      <center>
          <cfform action="campaign_add.cfm" method="post">
          <input name="campaign_add" type="submit" value="Add Campaign">
          </cfform>
      </center>
      <cfinclude template = "footer.cfm">


</body>
</html>
