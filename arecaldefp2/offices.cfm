<!DOCTYPE html>
<html>
<head>
    <cfinclude template = "./css/style.css">
</head>
<body>
    <cfparam name="projectName" default="Final Project" type="string">
    <cfparam name="Form.submit" default="AAA" type="string">
    <cfinclude template = "header.cfm">


<!-- if true, display a form -->
    <cfif #Form.submit# EQ "AAA">


    <!-- This query will be used in order to display the available categories in a table -->
    <cfquery name="getOffices"
             datasource="#Request.DSN#"
             username="#Request.username#"
             password="#Request.password#">
      SELECT * FROM tboffice
    </cfquery>


    <h4>Offices</h4>

    <!-- Display data in table -->
    <table>
         <tr><th>Officeid</th><th>Description</th></tr>
         <cfoutput query="getOffices">
         <cfform method="post">
         <tr>
          <input type="hidden" name="officeid" value="#officeid#">

          <input type="hidden" name="category" value="#category#">
          <input type="hidden" name="description" value="#description#">
          <td>#officeid#</td>
          <td>#description#</td>
          <td><input type="submit" name="submit" value="Update"></td>
          <td><input type="submit" name="submit" value="Delete"></td>
         </tr>
         </cfform>
        </cfoutput>
    </table>
    <center>
      <cfform action="office_add.cfm" method="post"><cfinput type="submit" name="office_add" value="Add New Office"></cfform>
    </center>




    <!-- if here, provide form to enable update -->
    <cfelseif #Form.submit# EQ "Update">


      <!-- this query will retrieve the category value that will be updated -->
      <cfquery name="updateOffice"
               datasource="#Request.DSN#"
               username="#Request.username#"
               password="#Request.password#">
      SELECT * FROM tboffice
      WHERE officeid = 
      <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.officeid#">
      </cfquery>


      <h4></h4>

    <table>
         <tr><th>Office</th></tr>
         <cfoutput query="updateOffice">
         <cfform action="office_update.cfm" method="post">
         <tr>
          <td>
            <!-- passing values to be used for processing the update -->
            <cfinput type="hidden" name="officeid" value="#officeid#">
            <cfinput type="hidden" name="oldDescription" value="#Description#">
            <cfinput name="newDescription" type="text"
              size="30" maxlength="30">
          </td>
          <td><input type="submit" name="submit" value="Update"</td>
         </tr>
         </cfform>
        </cfoutput>
    </table>



    


    <cfelseif #Form.submit# EQ "Delete">


      <cftry> 
            <cfquery name="deleteOffice"
                   datasource="#Request.DSN#"
                   username="#Request.username#"
                   password="#Request.password#">
          DELETE FROM tboffice
          WHERE officeid = 
          <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.officeid#">
          </cfquery>
         
          <h4>Delete Successful</h4>
          <center><a href="offices.cfm">Back</a></center>



        <!-- delete failed.. display message -->
      <cfcatch type="database"> 

            <h4>This database entry is parent to entries in other tables. Cannot delete.</h4>
            <center><a href="offices.cfm">Back</a></center>
            <cfabort>

      </cfcatch>
      </cftry>





    <cfelseif #Form.submit# EQ "Add New Office">
      <cfoutput><cflocation url="office_add.cfm"></cfoutput>


    <cfelse>
    <h4>There is a problem with the form</h4>
    <center><a href="offices.cfm">Back</a></center>


    </cfif>








    <cfinclude template = "footer.cfm">
</body>
</html>