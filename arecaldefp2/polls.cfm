<!DOCTYPE html>
<html>
<head>
    <cfinclude template = "./css/style.css">
</head>
<body>
    <cfparam name="projectName" default="Project4" type="string">
    <cfparam name="URL.pollid" default="AAA" type="string">
    <cfparam name="URL.results" default="AAA" type="string">
    <cfinclude template = "header.cfm">

<!-- the user has not selected a poll to participate in, so far -->
<cfif URL.pollid EQ "AAA">

    <!-- the user wants to see the results for a specific poll -->
    <cfif URL.results NEQ "AAA"> 

      <h4>Results of the poll:</h4>
      <!-- this query will display the current results for a specific poll -->
      <cfquery name="getResults"
               datasource="#Request.DSN#"
               username="#Request.username#"
               password="#Request.password#">
      SELECT candidatename, votes FROM
        (SELECT campaignid, count(campaignid) as votes
        FROM tbpollvote
        WHERE pollid =
        <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#URL.results#">
        GROUP BY campaignid) a
      INNER JOIN
        (SELECT campaignid, candidatename
        FROM tbcampaign a, tbcandidate b
        WHERE a.candidateid = b.candidateid) b
      ON a.campaignid = b.campaignid
      </cfquery>
      
      <table>
         <tr><th>Candidate</th><th>Votes</th></tr>      
         <cfoutput query="getResults">
         <tr>
          <td>#candidatename#</td>
          <td>#votes#</td>
         </tr>
        </cfoutput>
      </table>

        <!-- adding a pie char as a SPECIAL FEATURE -->
        <center>
        <cfchart  chartwidth="700"
                  chartheight="400"
                  xaxistitle="candidatename"
                  yaxistitle="votes"
                  dataBackgroundColor="##acb3b6"
                  foregroundColor="##33395a"
                  fontsize="18"
                  show3D="yes"
                  tipBGColor="##ccff99">
            <cfchartseries type="pie"
            query="getResults"
            valuecolumn="votes"
            itemcolumn="candidatename"
            colorlist="##ff00cc,##0033cc,##ffcc00,##9900cc">
        </cfchart>
        <center>






    <!-- if here, the user accessed the page without submitting any forms. Next, prompting user
         to select a particular poll -->
    <cfelse>  
      <cfquery name="getPolls"
             datasource="#Request.DSN#"
             username="#Request.username#"
             password="#Request.password#">
             SELECT pollid, pollname, p.areaid, a.description as area,
             p.officeid, o.description as office, term
             FROM tbpoll p, tboffice o, tbarea a
             WHERE  p.areaid = a.areaid
             AND    p.officeid = o.officeid
             ORDER BY 2
      </cfquery>


      <h4>Select a poll to participate in:</h4>
      <table>
           <tr><th>ID </th><th>Poll name</th><th>Area</th><th>Office</th><th>Term</th></tr>
          <cfoutput query="getPolls">
           <tr>
            <td>#pollid#</td>
            <td>#pollname#</td>
            <td>#office#</td>
            <td>#area#</td>
            <td>#term#</td>
            <td><a href="polls.cfm?pollid=#pollid#"><button>OK</button></a></td>
            </tr>
          </cfoutput>
      </table>
    </cfif>



<!-- if here, the user selected a specific poll to participate in -->
<cfelse>

    <!-- retrieving a specific poll's attributes for display -->
    <cfquery name="getPoll"
           datasource="#Request.DSN#"
           username="#Request.username#"
           password="#Request.password#">
           SELECT pollid, pollname, p.areaid, a.description as area,
           p.officeid, o.description as office, term
           FROM tbpoll p, tboffice o, tbarea a
           WHERE  p.areaid = a.areaid
           AND    p.officeid = o.officeid
           AND pollid = 
          <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#URL.pollid#">
    </cfquery>


    <h4>Choose your preferred option:</h4>
    <table>
         <tr><th>ID </th><th>Poll name</th><th>Area</th><th>Office</th><th>Term</th></tr>
        <cfoutput query="getPoll">
         <tr>
          <td>#pollid#</td>
          <td>#pollname#</td>
          <td>#office#</td>
          <td>#area#</td>
          <td>#term#</td>
          </tr>
        </cfoutput>
    </table>


 <!-- retrieving poll's candidates for voting -->
 <cfquery name="getCampaigns"
             datasource="#Request.DSN#"
             username="#Request.username#"
             password="#Request.password#">
      SELECT * FROM
      (SELECT campaignid, candidatename
      FROM tbcampaign a, tbcandidate b
      WHERE a.candidateid = b.candidateid) ab
      NATURAL JOIN
      tbpollcampaign pc
      WHERE pollid = 
      <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#URL.pollid#">
  </cfquery>
      


    <!-- form for voting -->
    <cfform action="poll_vote.cfm" method="post">

        <!-- no candidates have been added to the poll -->
        <cfif getCampaigns.RecordCount IS 0>
        There are no campaigns in poll<br>
        Please <a href="campaigns.cfm">try again</a>
        
        <!-- if here, there are candidates in this poll -->
        <cfelse>
        
        <table>
             <tr><th>Choose a Candidate</th></tr>      
            <cfoutput query="getCampaigns">
             <tr>
              <td>#candidatename#</td>
              <td><cfinput type="radio" name="c_id" value="#campaignid#"></td>
             </tr>
            </cfoutput>
        </table>
        <cfinput type="hidden" name="pollid" value="#URL.pollid#">
        <cfinput type="submit" name="submitVote" values="Submit">

        </cfif> <!-- campaigns RecordCount check, ends -->
    </cfform>

</cfif>

    <center><h4><a href="polls.cfm">Back</a></h4></center>
    <cfinclude template = "footer.cfm">
</body>
</html>