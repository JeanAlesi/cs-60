<!DOCTYPE html>
<html>
  <head>
    <title>Final Project</title>
    <cfinclude template = "./css/style.css">
  </head>
  <body>
    <cfparam name="Form.category" default="AAA" type="string">
    <cfparam name="projectName" default="Final Project" type="string">
    <cfinclude template = "header.cfm">

    
    <!-- area category not specfied... display all areas defined -->
    <cfif #Form.category# EQ "AAA">

      <cfquery name="getCategories"
               datasource="#Request.DSN#"
               username="#Request.username#"
               password="#Request.password#">
              SELECT * FROM tbareacategory
      </cfquery>

      <h4>Select an Area Category</h4>
      
      <table>
      <cfform method="post">
        <tr>
        <th>Select:</th>
        <td>
        <select name="category">
        <cfoutput query="getCategories">
        <option value="#category#">#category#</option>
        </cfoutput>
        </select>
        </td>
        <td>
          <cfinput name="submit" type="submit" value="View Areas" title="Select this category"></td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>
          <cfinput name="submit" type="submit" value=
          "Create Area" title="Create new area value in the selected category">
        </td>
        </tr>
      </cfform>
      </table>
      


    <!-- if here, an area category has been selected. Next, areas shall be displayed based on it -->
    <cfelse>

      <!-- if the Create Area button was pressed, redirect -->
      <cfif #Form.submit# EQ "Create Area">
        <cfoutput><cflocation url="area_create.cfm?category=#Form.category#"></cfoutput>
      </cfif>  <!-- end cfif on #Form.submit# -->


      <!-- this query will retrieve all areas defined within a certain category -->
      <cfquery name="getAreas"
               datasource="#Request.DSN#"
               username="#Request.username#"
               password="#Request.password#">
              SELECT areaid, description, category
              FROM tbarea
              WHERE category =
              <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.category#">

      </cfquery>

      <cfif getAreas.RecordCount EQ 0>
        <h4>There are no entries for the "<cfoutput>#Form.category#</cfoutput>" category in the database</h4>
      <cfelse>

        <h4><cfoutput>#getAreas.RecordCount#</cfoutput> 
          <cfif getAreas.RecordCount EQ 1>entry<cfelse>entries</cfif> found in the database</h4>


        <table>
           <tr><th>Description</th><th>Category</th></tr>
            <cfoutput query="getAreas">
            <tr>
            <td>#areaid#</td>
            <td>#description#</td>
            <td>#category#</td>
            <td>
              <!-- cfform inside td so that each form has its separate values -->
              <cfform action="area_delete.cfm" method="post">
              <cfinput type="hidden" name="areaid" value="#areaid#">
              <cfinput type="submit" name="submit" value="Delete">
              </cfform>
            </td>
            </tr>
            </cfoutput>
        </table>

      </cfif>
        <center>
          <br><a href="areas.cfm">Back</a><br>
        </center>
      

  </cfif>
  

  <cfinclude template = "footer.cfm">
</body>
</html>