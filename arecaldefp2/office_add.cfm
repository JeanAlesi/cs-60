<!DOCTYPE html>
<html>
<head>
    <cfinclude template = "./css/style.css">
</head>
<body>
    <cfparam name="projectName" default="Final Project" type="string">
    <cfinclude template = "header.cfm">


    	<!-- if true, no form has been submitted yet -->
    <cfif NOT isDefined("Form.submit")>


      <cfquery name="getAreaCategories"
             datasource="#Request.DSN#"
             username="#Request.username#"
             password="#Request.password#">
      select * from tbareacategory
      </cfquery>


    	<h4>Enter new Office name:</h4>
    	<center>
    	<cfform method="post">
    		<cfinput name="newOffice" type="text"
              size="30" maxlength="30">

        <select name="category">
        <cfoutput query="getAreaCategories">
        <option value="#category#">#category#</option>
        </cfoutput>
        </select>

        <cfinput name="submit" type="submit" value="Submit">
        </cfform>
    	</center>



    <!-- if here, a form has been submitted.. process add -->
    <cfelse>
    	<!-- further validation -->
    	<cfif Trim("Form.newOffice") EQ "">
        	<h4>Please enter a valid office name. <a href="offices.cfm">Back</a></h4>
      <cfelse>

          <cfquery name="addOffice"
                   datasource="#Request.DSN#"
                   username="#Request.username#"
                   password="#Request.password#">
          INSERT into tboffice values ('AAA', <!--- AAA because trigger will handle the sequencing--->
          <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.newOffice#">,
          <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.category#">) 
          </cfquery>

          <h4>Insert Successful</h4>


      </cfif> <!-- end trim check -->
          
    </cfif>

    	<h4><a href="offices.cfm">Back</a></h4>




    <cfinclude template = "footer.cfm">
</body>
</html>