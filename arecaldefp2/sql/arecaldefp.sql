-- ******************************************************
-- arecaldefp.sql
--
-- Database for FINAL PROJECT
--
-- Author:  Antonio Recalde Russo
--
-- Last Modified:   December, 2014
--
-- ******************************************************





-- ******************************************************
--		NOTE TO GRADER
-- ******************************************************
-- The ranges for primary keys are purposedly low. This is
-- so in order to facilitate testing. 
-- I am aware that if I were to release this application
-- to public use, I would have to modify that.




-- ******************************************************
--		SPOOL SESSION
-- ******************************************************

spool fp.lst


-- ******************************************************
--		DROP TABLES
-- ******************************************************
	DROP table tblogin PURGE;
	DROP table tbpollvote PURGE;
	DROP table tbpollcampaign PURGE;
	DROP table tbpoll PURGE;
	DROP table tbcampaign PURGE;
	DROP table tbseat PURGE;
	DROP table tbarea PURGE;
	DROP table tboffice PURGE;
	DROP table tbareacategory PURGE;
	DROP table tbcandidate PURGE;

-- ******************************************************
-- 		DROP TRIGGERS
-- ******************************************************




-- ******************************************************
--		DROP SEQUENCES
-- ******************************************************

	DROP sequence seq_candidateid;
	DROP sequence seq_campaignid;
	DROP sequence seq_pollid;
	DROP sequence seq_pollvoteid;
	DROP sequence seq_areaid;


-- ******************************************************
--		CREATE TABLES
-- ******************************************************

CREATE table tbcandidate (
	candidateid			number(5,0)		not null
		constraint pk_tbcandidate primary key,
	candidatename		varchar2(30)	not null
);

CREATE table tbareacategory (
	category 			char(10)		not null
	constraint pk_tbareacategory primary key
);

CREATE table tboffice (
	officeid			char(3)			not null
		constraint rg_officeid check (officeid between '000' and '999')
		constraint pk_officeid primary key,
	description			varchar2(30)	not null,
	category 			char(10)		not null
		constraint fk_tboffice_tbareacat references tbareacategory (category)			
);

CREATE table tbarea (
	areaid				char(3)			not null
		constraint rg_areaid check (areaid between '000' and '999')
		constraint pk_areaid primary key,
	description			varchar2(32)	not null,
	category 			char(10)		not null
		constraint fk_tbarea_tbareacategory references tbareacategory (category)
);

CREATE table tbseat (
	areaid  			char(3)			not null
		constraint fk_areaid_tbseat references tbarea (areaid),
	officeid 			char(3)			not null
		constraint fk_officeid references tboffice (officeid),
	term 				char(2)			not null,
		constraint pk_tbseat primary key (areaid, officeid, term)
);

CREATE table tbcampaign (
	campaignid			number(5,0)		not null
		constraint pk_tbcampaign primary key,
	candidateid			number(5,0)		not null
		constraint fk_tbcampaign_tbcandidate references tbcandidate (candidateid)
		on delete CASCADE,
	areaid				char(3)			not null,
	officeid			char(3)			not null,
	term				char(2)			not null,
		constraint fk_tbcampaign_tbseat foreign key (areaid, officeid, term)
		references tbseat (areaid, officeid, term),
		constraint uq_candidate_seat UNIQUE(candidateid,areaid, officeid, term)
);

CREATE table tbpoll (
	pollid				char(3)			not null
		constraint rg_pollid check (pollid between '000' and '999')
		constraint pk_tbpoll primary key,
	pollname			char(16)		not null,
	areaid  			char(3)			not null,
	officeid 			char(3)			not null,
	term 				char(2)			not null,
		constraint fk_tbpoll_tbseat foreign key (areaid, officeid, term)
		references tbseat (areaid, officeid, term) on delete CASCADE,
	active				char(3)			not null			
);

CREATE table tbpollcampaign (
  pollid        char(3)     not null
    constraint fk_tbpollcamp_tbpoll references tbpoll (pollid) on delete CASCADE,
  campaignid      number(5,0)   not null
    constraint fk_tbpollcamp_tbcampaign references tbcampaign (campaignid) on delete CASCADE,
    constraint pk_tbpollcampaigns primary key (pollid, campaignid)
);

CREATE table tbpollvote (
	pollvoteid			char(10)		not null
		constraint		rg_pollvoteid check (pollvoteid between '0000000000' and '9999999999')
		constraint		pk_tbpollvote primary key,
	pollid 				char(3)			not null
		constraint fk_tbpollvote_tbpoll references tbpoll (pollid)
		on delete CASCADE,
	campaignid 			number(5,0)		not null
		constraint fk_tbpollvote_tbcampaign references tbcampaign (campaignid)
		on delete CASCADE
);

 CREATE table tblogin (
 	uname 				varchar2(20) 	not null,
 	pwd 				varchar2(20) 	not null,
 	fname 				varchar2(20) 	not null,
 	userview 			varchar2(20) 	null
 );


-- ******************************************************
--    CREATE SEQUENCES
-- ******************************************************

   CREATE sequence seq_candidateid
	increment by 1
	start with 0
    minvalue 0;

   CREATE sequence seq_campaignid
    increment by 1
    start with 0
    minvalue 0;

   CREATE sequence seq_pollid
    increment by 1
    start with 0
    minvalue 0; 

   CREATE sequence seq_pollvoteid
    increment by 1
    start with 0
    minvalue 0;

-- Values 0 to 99 reserved for hardcoding
   CREATE sequence seq_areaid
    increment by 1
    start with 100
    minvalue 100;

-- Values 0 to 99 reserved
   CREATE sequence seq_officeid
    increment by 1
    start with 100
    minvalue 100;


-- ****************************************************************
--  POPULATE TABLES
-- ****************************************************************

INSERT into tblogin values ('user', 'password', 'user', 'ALL');


INSERT into tbcandidate values (seq_candidateid.nextval, 'Barack Hussein Obama');
INSERT into tbcandidate values (seq_candidateid.nextval, 'Mitt Romney');
INSERT into tbcandidate values (seq_candidateid.nextval, 'Hillary Clinton');

INSERT into tbareacategory values ('country');
INSERT into tbareacategory values ('state');
INSERT into tbareacategory values ('city');


INSERT into tbarea values ('000', 'United States of America', 'country');
INSERT into tbarea values ('001', 'Alabama', 'state');
INSERT into tbarea values ('002', 'Alaska', 'state');
INSERT into tbarea values ('003', 'Arizona', 'state');
INSERT into tbarea values ('004', 'Arkansas', 'state');
INSERT into tbarea values ('005', 'California', 'state');
INSERT into tbarea values ('006', 'Colorado', 'state');
INSERT into tbarea values ('007', 'Connecticut', 'state');
INSERT into tbarea values ('008', 'Delaware', 'state');
INSERT into tbarea values ('009', 'District of Columbia', 'state');
INSERT into tbarea values ('010', 'Florida', 'state');
INSERT into tbarea values ('011', 'Georgia', 'state');
INSERT into tbarea values ('012', 'Hawaii', 'state');
INSERT into tbarea values ('013', 'Idaho', 'state');
INSERT into tbarea values ('014', 'Illinois', 'state');
INSERT into tbarea values ('015', 'Indiana', 'state');
INSERT into tbarea values ('016', 'Iowa', 'state');
INSERT into tbarea values ('017', 'Kansas', 'state');
INSERT into tbarea values ('018', 'Kentucky', 'state');
INSERT into tbarea values ('019', 'Louisiana', 'state');
INSERT into tbarea values ('020', 'Maine', 'state');
INSERT into tbarea values ('021', 'Maryland', 'state');
INSERT into tbarea values ('022', 'Massachusetts', 'state');
INSERT into tbarea values ('023', 'Michigan', 'state');
INSERT into tbarea values ('024', 'Minnesota', 'state');
INSERT into tbarea values ('025', 'Mississippi', 'state');
INSERT into tbarea values ('026', 'Missouri', 'state');
INSERT into tbarea values ('027', 'Montana', 'state');
INSERT into tbarea values ('028', 'Nebraska', 'state');
INSERT into tbarea values ('029', 'Nevada', 'state');
INSERT into tbarea values ('030', 'New Hampshire', 'state');
INSERT into tbarea values ('031', 'New Jersey', 'state');
INSERT into tbarea values ('032', 'New Mexico', 'state');
INSERT into tbarea values ('033', 'New York', 'state');
INSERT into tbarea values ('034', 'North Carolina', 'state');
INSERT into tbarea values ('035', 'North Dakota', 'state');
INSERT into tbarea values ('036', 'Ohio', 'state');
INSERT into tbarea values ('037', 'Oklahoma', 'state');
INSERT into tbarea values ('038', 'Oregon', 'state');
INSERT into tbarea values ('039', 'Pennsylvania', 'state');
INSERT into tbarea values ('040', 'Rhode Island', 'state');
INSERT into tbarea values ('041', 'South Carolina', 'state');
INSERT into tbarea values ('042', 'South Dakota', 'state');
INSERT into tbarea values ('043', 'Tennessee', 'state');
INSERT into tbarea values ('044', 'Texas', 'state');
INSERT into tbarea values ('045', 'Utah', 'state');
INSERT into tbarea values ('046', 'Vermont', 'state');
INSERT into tbarea values ('047', 'Virginia', 'state');
INSERT into tbarea values ('048', 'Washington', 'state');
INSERT into tbarea values ('049', 'West Virginia', 'state');
INSERT into tbarea values ('050', 'Wisconsin', 'state');
INSERT into tbarea values ('051', 'Wyoming', 'state');

INSERT into tboffice values ('000', 'President', 'country');
INSERT into tboffice values ('001', 'Governor', 'state');
INSERT into tboffice values ('002', 'Congress', 'state');
INSERT into tboffice values ('003', 'Mayor', 'city');
INSERT into tboffice values ('004', 'City Council', 'city');

/* attributes: areaid, officeid, term  */
INSERT into tbseat values ('000', '000', '12');
INSERT into tbseat values ('000', '000', '16');
INSERT into tbseat values ('005', '001', '14');
INSERT into tbseat values ('001', '001', '14');

/* attributes: campaignid, candidateid, areaid, office, term */
INSERT into tbcampaign values (seq_campaignid.nextval, 1, '000', '000', '12');
INSERT into tbcampaign values (seq_campaignid.nextval, 3, '000', '000', '16');
INSERT into tbcampaign values (seq_campaignid.nextval, 2, '000', '000', '12');
INSERT into tbcampaign values (seq_campaignid.nextval, 2, '000', '000', '16');

INSERT into tbpoll values (seq_pollid.nextval, 'default', '000', '000', '16', 'yes');

INSERT into tbpollcampaign values (seq_pollid.currval, 2);
INSERT into tbpollcampaign values (seq_pollid.currval, 4);



-- ******************************************************
--    CREATE TRIGGERS
-- ******************************************************

/* The following trigger (cat_match) will ensure that there
   isn't a category mismatch when creating a seat.
   E.g.: President of Arizona,
   		 Mayor of Russia,
   		 Governor of the United States,
   are all category mismatches. 

You can use the following value to TEST trigger CAT_MATCH
   INSERT into tbseat values ('000', '001', '12');  */

   CREATE OR REPLACE trigger CAT_MATCH
   AFTER INSERT OR UPDATE OR DELETE ON tbseat
   FOR EACH ROW
   declare
      x number;
      category_mismatch EXCEPTION;
   BEGIN
      SELECT count(*) into x 
      FROM
      (SELECT category FROM tboffice WHERE officeid = :new.officeid) a,
      (SELECT category FROM tbarea WHERE areaid = :new.areaid) b 
      WHERE a.category = b.category;

      if x = 0 then
      raise_application_error(-20000,'Dont do that');
         
      end if;
   end CAT_MATCH;
/  


--  The trigger tr_areaid will make it possible to auto-increment areaid, which is a char
	CREATE OR REPLACE trigger TR_AREAID
	BEFORE INSERT ON tbarea
	FOR EACH ROW
	BEGIN
		SELECT seq_areaid.nextval into :new.areaid FROM dual;
	end TR_AREAID;
/


--  The trigger tr_officeid will make it possible to auto-increment officeid, which is a char
	CREATE OR REPLACE trigger TR_OFFICEID
	BEFORE INSERT ON tboffice
	FOR EACH ROW
	BEGIN
		SELECT seq_officeid.nextval into :new.officeid FROM dual;
	end TR_OFFICEID;
/


