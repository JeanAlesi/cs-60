<!DOCTYPE html>
<html>
  <head>
    <title>PSET4 - Project 4</title>
    <cfinclude template = "./css/style.css">
  </head>
  <body>
    <cfparam name="Form.candidateid" default="AAA" type="string">
    <cfparam name="projectName" default="Final Project" type="string">
    <cfinclude template = "header.cfm">


      <!-- if true, a candidate has been selected. Now we have to select what he'll be running for -->
      <cfif isdefined("Form.add")> 

        <!-- this selects all available seats this candidate can run for -->
        <cfquery name="getSeat"
             datasource="#Request.DSN#"
             username="#Request.username#"
             password="#Request.password#">
             SELECT o.description AS office, a.description as area, s.officeid, s.areaid, term 
             FROM tbarea a, tboffice o, tbseat s
             WHERE a.areaid = s.areaid
             AND o.officeid = s.officeid
             ORDER BY 1 DESC
        </cfquery>


    <h4>Select:</h4>
    <table>
         <tr><th>Area</th><th>Office</th><th>Term</th></tr>
        <cfoutput query="getSeat">
         <tr>
          <td>#office#</td>
          <td>#area#</td>
          <td>#term#</td>
          <td>
              <cfform action="" method="post">
              <input type="hidden" name="candidateid" value="#Form.candidateid#">
              <input type="hidden" name="areaid" value="#areaid#">
              <input type="hidden" name="officeid" value="#officeid#">
              <input type="hidden" name="term" value="#term#">
              <input name="add2" type="submit" value="create">
              </cfform>
          </td>
          </tr>
        </cfoutput>
    </table>

    
    <!-- if true, we insert into the campaign table -->
    <cfelseif isdefined("Form.add2")>

      <cfquery  name="insertCampaign"
                datasource="#Request.DSN#"
                username="#Request.username#"
                password="#Request.password#">
                INSERT into tbcampaign values
                (seq_campaignid.nextval, 
                <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#Form.candidateid#">,
                <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.areaid#">,
                <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.officeid#">,
                <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.term#">
                )
      </cfquery>
      <cfoutput><cflocation url="campaigns.cfm?candidateid=#Form.candidateid#"></cfoutput>


    <cfelse> <!-- candidate id EQ "AAA". Next, a candidate must be selected in order to create a campaign -->

      <!-- getting candidates to display in table -->
      <cfquery name="getCandidate"
             datasource="#Request.DSN#"
             username="#Request.username#"
             password="#Request.password#">
      select * from tbcandidate
      </cfquery>


      <h4>Select a Candidate:</h4>

      <div class="wrapper">
      <!-- this form will select the candidate running in this campain -->
      <form action="" method="post">  
      <table>
      <tr>
      <th>Candidate:</th>
      <td>
      <select name="candidateid">
      <cfoutput query="getCandidate">
      <option value="#candidateid#">#candidatename#</option>
      </cfoutput>
      </select>
      </td>
      </tr>
      <tr>
      <td>&nbsp;</td>
      <td>
      <input name="add" type="submit" Value="Ok"/>
      </td>
      </tr>
      </table>
      </form>
    </div>
    
    </cfif>

 <cfinclude template = "footer.cfm">
  </body>
</html>