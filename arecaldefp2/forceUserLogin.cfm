<cflogin>

<cfif not (isDefined("FORM.userLogin") and isDefined("FORM.userPassword"))>
	<cfinclude template="login.cfm">
	<cfabort>


<cfelse>
	<cfquery name="getUser"
	datasource="#Request.DSN#"
	username="#Request.username#"
	password="#Request.password#">
	select *
	from tblogin
	where uname = <cfqueryparam
	cfsqltype="CF_SQL_VARCHAR"
	value="#Form.UserLogin#"> and
	pwd = <cfqueryparam cfsqltype="CF_SQL_VARCHAR"
	value="#Form.UserPassword#">
	</cfquery>


<cfif getUser.recordCount EQ 1>
	<cfloginuser
	name="#getUser.fname#"
	password="#FORM.userPassword#"
	roles="#getUser.userview#">


<cfelse>
	<div class="errorMsg">
	Sorry, the username and password combination
	are not recognized.<br />
	Please try again.</div>
	<cfinclude template="login.cfm">
	<cfabort>

</cfif>
</cfif>
</cflogin>