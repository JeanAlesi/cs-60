<!DOCTYPE html>
<html>
<head>
	
</head>
<body>
  <cfparam name="projectName" default="Project4" type="string">
  <cfinclude template = "header.cfm">



  <!-- update option selected for a given candidate -->
	<cfif isdefined("Form.update")>
		<cfquery  name="updateCandidate"
              	  datasource="#Request.DSN#"
              	  username="#Request.username#"
                  password="#Request.password#"
                  result="updateResult">
       	UPDATE tbcandidate
       	SET candidatename = 
       		<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.candidatename#">
        WHERE candidateid =
          <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.candidateid#">
    	</cfquery>
	
		<cfoutput> <!-- redirect to show the list of candidates.. changes must already be in place -->
	    	<cflocation url="candidates.cfm">
	    </cfoutput>
	

	<cfelse> <!-- displaying list of candidates -->

	 <cfquery  name="fetchCandidate"
             datasource="#Request.DSN#"
             username="#Request.username#"
             password="#Request.password#">
       	SELECT * FROM tbcandidate 
        WHERE candidateid = 
          <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.candidateid#">
    </cfquery>



  <cfform method="post">
    <table>
      <tr>
      <th>ID</th>
      <th>Name</th>
      </tr>

      <tr>
    	<cfoutput>
      <td>#fetchCandidate.candidateid#</td>
      <td><input name="candidatename" type="text" value="#fetchCandidate.candidatename#"
      	size="30" maxlength="30"></td>
      <td>
         	<input type="hidden" name="candidateid" value="#Form.candidateid#">
          <input name="update" type="submit" value="Update">
      </td>
      </cfoutput>
      </tr>
      
   </table>
  </cfform>
	
</cfif>

<cfinclude template = "footer.cfm">



</body>
</html>