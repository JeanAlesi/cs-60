<!DOCTYPE html>
<html>
<head>
    <cfinclude template = "./css/style.css">
</head>
<body>
    <cfparam name="projectName" default="Final Project" type="string">
    <cfinclude template = "header.cfm">



    	<cfif isDefined("Form.category")>
      	
        <cftry> 
            <cfquery name="deleteCategory"
                   datasource="#Request.DSN#"
                   username="#Request.username#"
                   password="#Request.password#">
          DELETE FROM tbareacategory
          WHERE category = 
          <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.Category#">
          </cfquery>
         
          <h4>Delete Successful</h4>


        <!-- delete failed.. display message -->
        <cfcatch type="database"> 

            <h4>This database entry is parent to entries in other tables, or there is another entry with the same name.</h4>
            <center><a href="categories.cfm">Back</a></center>
            <cfabort>

        </cfcatch>
        </cftry>

      </cfif> <!-- Trim string -->

    <cfelse>
      <h4>Sorry, there is a problem with the Form</h4>

    </cfif>
      <center><a href="categories.cfm">Back</a></center>





    <cfinclude template = "footer.cfm">
</body>
</html>