<!DOCTYPE html>
<html>
<head>
    <cfinclude template = "./css/style.css">
</head>
<body>
    <cfparam name="projectName" default="Final Project" type="string">
    <cfinclude template = "header.cfm">


<!-- if true, form was sent with a seat from tbseat selected (areaid, officeid, term) -->
<cfif isdefined("Form.createPoll")>
   <!-- display campaigns currently running for the selected seat/position -->
  <cfquery name="getCampaigns"
             datasource="#Request.DSN#"
             username="#Request.username#"
             password="#Request.password#">
      SELECT campaignid, can.candidateid, candidatename, o.description as office, a.description area, term 
      FROM tbcandidate can, tboffice o, tbarea a, tbcampaign cam
      WHERE can.candidateid = cam.candidateid
      AND   o.officeid = cam.officeid
      AND   a.areaid = cam.areaid
      AND   cam.officeid = 
      <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.officeid#">
      AND   cam.areaid = 
      <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.areaid#">
      AND   cam.term = 
      <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.term#">
      ORDER BY 3
    </cfquery>

    <cfform action="" method="post">
    <cfoutput>
        <input type="hidden" name="areaid" value="#areaid#">
        <input type="hidden" name="officeid" value="#officeid#">
        <input type="hidden" name="term" value="#term#">
        <h4>Select Poll name</h4>
        <cfinput type="text" name="pollname" size="10" 
                 maxlength="10"
                 validateat="onSubmit"
                 required="Yes"
                 message="Enter Poll name">
    </cfoutput>

    <h4>Choose Campaigns to participate in poll</h4>
       
        <cfif getCampaigns.RecordCount IS 0>
        There are no campaigns running for that position<br>
        Please <a href="campaigns.cfm">try again</a>
        </cfif>

        <!-- admin can chose campaigns to be in the poll through checkbox -->
        <table>
             <tr><th>ID</th><th>Candidate</th><th>Running For</th><th>Area</th><th>Term</th></tr>
            <cfoutput query="getCampaigns">
             <tr>
              <td>#campaignid#</td>
              <td>#candidatename#</td>
              <td>#office#</td>
              <td>#area#</td>
              <td>#term#</td>
              <td><cfinput type="checkbox" name="c_id" value="#campaignid#"></td>
             </tr>
            </cfoutput>
        </table>
        <input type="submit" name="createPoll2" values="submit">
    </cfform>




<cfelseif isdefined("Form.createPoll2")>

  <cfif Trim("#Form.pollname#") EQ "">
    <h4>Please enter a valid name. <a href="poll_create.cfm">Try again.</a></h4>

  <cfelse>
      <cfquery name="insertPoll"
             datasource="#Request.DSN#"
             username="#Request.username#"
             password="#Request.password#">
             INSERT into tbpoll values (seq_pollid.nextval,
      <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.pollname#">,
      <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.areaid#">, 
      <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.officeid#">, 
      <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.term#">,
             'yes')
      </cfquery>


      <cfif isdefined("Form.c_id")>
      <cfloop index = "ListElement" list = "#Form.c_id#">
      <cfquery datasource="#Request.DSN#"
               username="#Request.username#"
               password="#Request.password#">
               INSERT into tbpollcampaign values (seq_pollid.currval,
               <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ListElement#">)
      </cfquery>
      </cfloop>
      </cfif>
      <cfoutput><cflocation url="dev_polls.cfm"></cfoutput>
    </cfif>

<cfelse>

	   <cfquery name="getSeats"
             datasource="#Request.DSN#"
             username="#Request.username#"
             password="#Request.password#">
             SELECT o.description AS office, a.description as area, s.officeid, s.areaid, term 
             FROM tbarea a, tboffice o, tbseat s
             WHERE a.areaid = s.areaid
             AND o.officeid = s.officeid
             ORDER BY 1 DESC
    </cfquery>
    
    <h4>Select a seat:</h4>
    <table>
         <tr><th>Office</th><th>Area</th><th>Term</th></tr>
        <cfoutput query="getSeats">
         <tr>
          <td>#office#</td>
          <td>#area#</td>
          <td>#term#</td>
          <td>
              <cfform action="" method="post">
              <input type="hidden" name="area" value="#area#">
              <input type="hidden" name="areaid" value="#areaid#">
              <input type="hidden" name="office" value="#office#">
              <input type="hidden" name="officeid" value="#officeid#">
              <input type="hidden" name="term" value="#term#">
              <input name="createPoll" type="submit" value="Select">
              </cfform>
          </td>
          </tr>
        </cfoutput>
    </table>
</cfif>

    <cfinclude template = "footer.cfm">
</body>
</html>