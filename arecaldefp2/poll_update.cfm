<!DOCTYPE html>
<html>
<head>
    <cfinclude template = "./css/style.css">
</head>
<body>
    <cfparam name="projectName" default="Project4" type="string">
    <cfparam name="URL.pollid" default="AAA" type="string">
    <cfinclude template = "header.cfm">

<!-- no poll has been selected prompt admin to do so -->
<cfif URL.pollid EQ "AAA">

<h4>Please select a poll. <a href="dev_polls.cfm">Back</a></h4>


<!-- pollid = AAA but, form has been submitted -->
<cfelseif isdefined("Form.addCandidate")>
    <!-- This query will allow us to display only the candidates that can participate in this poll.. 
          Namely, they are the candidates that are running for the seat that has been selected.. The values are displayed with their descriptive names (instead of id)found in other tables -->
    <cfquery name="getCampaigns"
             datasource="#Request.DSN#"
             username="#Request.username#"
             password="#Request.password#">
      SELECT * from
        (SELECT campaignid, can.candidateid, candidatename, o.description as office, a.description area, term 
        FROM tbcandidate can, tboffice o, tbarea a, tbcampaign cam
        WHERE can.candidateid = cam.candidateid
        AND   o.officeid = cam.officeid
        AND   a.areaid = cam.areaid
        AND   cam.officeid = 
        <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.officeid#">
        AND   cam.areaid = 
        <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.areaid#">
        AND   cam.term = 
        <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.term#">)
      LEFT JOIN
        (SELECT pc.campaignid AS cid 
        FROM tbpollcampaign pc
        WHERE pc.pollid =
        <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#URL.pollid#">)
        ON campaignid = cid
        WHERE cid IS NULL
    </cfquery>

    <cfform action="" method="post">
        <h4>Choose Campaigns</h4>
       
        <cfif getCampaigns.RecordCount IS 0>
        There are no additional campaigns running for that position<br>
        Please <a href="campaigns.cfm">add a campaign</a>to the race.
        </cfif>

        
        <table>
             <tr><th>ID</th><th>Candidate</th><th>Running For</th><th>Area</th><th>Term</th></tr>
            <cfoutput query="getCampaigns">
             <tr>
              <td>#campaignid#</td>
              <td>#candidatename#</td>
              <td>#office#</td>
              <td>#area#</td>
              <td>#term#</td>
              <td><cfinput type="checkbox" name="c_id" value="#campaignid#"></td>
             </tr>
            </cfoutput>
        </table>
        <input type="submit" name="addCandidate2" values="submit">
    </cfform>

    <!-- second stage in adding candidates to the poll. This will proceed to insert the selected values" -->
    <cfelseif isdefined("Form.addCandidate2")>
     
      <cfif isdefined("Form.c_id")>

      <!-- I'm putting the "c_id" variable into a list, so I can run a for loop and insert all of the values into 
           the database table -->  
      <cfloop index = "ListElement" list = "#Form.c_id#">
      <!-- so, this query will run as many timesa as there are elements in the list -->  
      <cfquery datasource="#Request.DSN#"
               username="#Request.username#"
               password="#Request.password#">
               INSERT into tbpollcampaign values (
               <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#URL.pollid#">,
               <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#ListElement#">)
      </cfquery>
      </cfloop>
      </cfif>
      <cfoutput><cflocation url="dev_polls.cfm"></cfoutput>





























<cfelse>

    <cfquery name="getPoll"
             datasource="#Request.DSN#"
             username="#Request.username#"
             password="#Request.password#">
             SELECT pollid, pollname, p.areaid, a.description as area,
             p.officeid, o.description as office, term
             FROM tbpoll p, tboffice o, tbarea a
             WHERE  p.areaid = a.areaid
             AND    p.officeid = o.officeid
             AND    p.pollid = 
             <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#URL.pollid#">
             ORDER BY 2
    </cfquery>

    <table>
         <tr><th>ID </th><th>Poll name</th><th>Area</th><th>Office</th><th>Term</th></tr>
        <cfoutput query="getPoll">
         <tr>
          <td>#pollid#</td>
          <td>#pollname#</td>
          <td>#office#</td>
          <td>#area#</td>
          <td>#term#</td>
          </tr>
        </cfoutput>
    </table>



    <cfquery name="getPollCampaigns"
             datasource="#Request.DSN#"
             username="#Request.username#"
             password="#Request.password#">
             SELECT pc.campaignid, candidatename 
             FROM tbpollcampaign pc, tbcampaign c, tbcandidate can
             WHERE (pc.campaignid = c.campaignid AND can.candidateid = c.candidateid)
             AND pollid = 
             <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#URL.pollid#">
    </cfquery>
    <br>
    <h4>Candidates in this poll</h4>
    <table>
         <cfoutput query="getPollCampaigns">
         <tr>
          <td>#candidatename#</td>
          <td>
          <cfform method="post">
              <input type="hidden" name="pollid" value="#pollid#">
              <input type="submit" name="removeCandidate" value="Remove">
              </cfform>
          </td>
          </tr>
        </cfoutput>
    </table>

    <center>
          <cfoutput>
          <cfform action="" method="post">
          <input type="hidden" name="officeid" value="#getPoll.officeid#">
          <input type="hidden" name="areaid" value="#getPoll.areaid#">
          <input type="hidden" name="term" value="#getPoll.term#">
          <input name="addCandidate" type="submit" value="Add another Candidate">
          </cfform>
          </cfoutput>
    </center>




</cfif>

    <cfinclude template = "footer.cfm">
</body>
</html>