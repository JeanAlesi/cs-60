<!DOCTYPE html>
<html>
<head>
    <cfinclude template = "./css/style.css">
</head>
<body>
    <cfparam name="projectName" default="Final Project" type="string">
    <cfinclude template = "header.cfm">


    <!-- if true, no form has been submitted yet -->
    <cfif NOT isDefined("Form.submit")>
    	<h4>Enter new Category name:</h4>
    	<center>
    	<cfform method="post">
    		<cfinput name="newCategory" type="text"
              size="30" maxlength="30">
            <cfinput name="submit" type="submit" value="Submit">
        </cfform>
    	</center>

    <!-- if here, a form has been submitted.. process add -->
    <cfelse>
    	<!-- further validation -->
    	<cfif Trim("Form.newCategory") EQ "">
        	<h4>Please enter a valid category name. <a href="categories.cfm">Back</a></h4>
      	<cfelse>

          <cftry>  <!-- start try block for INSERT -->

          <cfquery name="addCategory"
                   datasource="#Request.DSN#"
                   username="#Request.username#"
                   password="#Request.password#">
          INSERT into tbareacategory values (
          <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.newCategory#">) 
          </cfquery>

          <h4>Insert Successful</h4>


          <cfcatch type="database"> <!-- error in insert -->
            <h4>Insert not successful</h4>
            <center><a href="categories.cfm">Back</a></center>
            <cfabort>

        </cfcatch>
        </cftry>


      	</cfif> <!-- end trim check -->
          
    </cfif>

    	<h4><a href="categories.cfm">Back</a></h4>


    <cfinclude template = "footer.cfm">
</body>
</html>