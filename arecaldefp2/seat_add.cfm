<!DOCTYPE html>
<html>
  <head>
    <title>PSET4 - Project 4</title>
    <cfinclude template = "./css/style.css">
  </head>
  <body>
    <cfparam name="projectName" default="Project4" type="string">
    <cfinclude template = "header.cfm">


  <cfif isdefined("Form.seat_add")>

      <!-- this query will be used to display the offices available -->
      <cfquery name="getOffice"
             datasource="#Request.DSN#"
             username="#Request.username#"
             password="#Request.password#">
      SELECT officeid, description FROM tboffice
      WHERE category = 
      <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.areacategory#">
      </cfquery>
      
      <!-- this query will be used to display the areas available for that office -->
      <cfquery name="getArea"
             datasource="#Request.DSN#"
             username="#Request.username#"
             password="#Request.password#">
      SELECT * FROM tbarea
      WHERE category = <!-- categories have to match... e.g. President -> country, Mayor -> city... not President -> city -->
      <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.areacategory#">
      </cfquery>


      <h4>Select:</h4>

    <div class="wrapper">
      <cfform action="" method="post">
      <table>
      <tr>
            <th>Office</th>
            <th>Area</th>
            <th>Term</th>
      </tr>
      <tr>
      <td>
            <select name="officeid">
            <cfoutput query="getOffice">
            <option value="#officeid#">#description#</option>
            </cfoutput>
            </select>
      </td>
      <td>
            <select name="areaid">
            <cfoutput query="getArea">
            <option value="#areaid#">#description#</option>
            </cfoutput>
            </select>
      </td>
      <td>
            <cfinput name="term" type="text" size="2" maxlength="2">
      </td>
      <td>
            <input name="seat_add2" type="submit" Value="Ok"/>
      </td>
      </tr>
      </table>
      </cfform>
    </div>

   <!-- second stage in adding a seat -->
   <cfelseif isdefined("Form.seat_add2")>

      <!-- this query will be used to check if there are already entries with the same values -->
      <cfquery  name="checkDatabase"
                datasource="#Request.DSN#"
                username="#Request.username#"
                password="#Request.password#">
      SELECT * FROM tbseat
      WHERE officeid =
      <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.officeid#">
      AND areaid = 
      <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.areaid#">
      AND term = 
      <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.term#">
      </cfquery>



   <cfif checkDatabase.RecordCount IS 0>

      <cfquery  name="getAreaCategory"
                datasource="#Request.DSN#"
                username="#Request.username#"
                password="#Request.password#">
      INSERT into tbseat values (
      <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.areaid#">,
      <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.officeid#">,
      <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.term#">)
      </cfquery>

      <cfoutput>
         <cflocation url="seats.cfm">
      </cfoutput>
   
   <!-- if here, entry already exists -->
   <cfelse>
      <center><h4>Data already found in database.</h4><a href="seats.cfm">Back</a>
   </cfif>


   <!-- if here, no form has been submitted yet. First we have to select what area category we want
        to add a seat for -->
  <cfelse>

      <cfquery name="getAreaCategory"
             datasource="#Request.DSN#"
             username="#Request.username#"
             password="#Request.password#">
      select * from tbareacategory
      </cfquery>


      <div class="wrapper">
         <form action="" method="post">
         <table>
         <tr>
         <th>Area Category:</th>
         <td>
         <select name="areaCategory">
         <cfoutput query="getAreaCategory">
         <option value="#category#">#category#</option>
         </cfoutput>
         </select>
         </td>
         </tr>
         <tr>
         <td>&nbsp;</td>
         <td>
         <input name="seat_add" type="submit" Value="Ok"/>
         </td>
         </tr>
         </table>
         </form>
      </div>

  </cfif>

 <cfinclude template = "footer.cfm">
  </body>
</html>