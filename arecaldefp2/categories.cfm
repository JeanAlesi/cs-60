<!DOCTYPE html>
<html>
  <head>
    <title>Final Project</title>
    <cfinclude template = "./css/style.css">
  </head>
  <body>


    <cfparam name="projectName" default="Final Project" type="string">
    <cfparam name="Form.submit" default="AAA" type="string">

    <cfinclude template = "header.cfm">



    <!-- if true, display a form -->
    <cfif #Form.submit# EQ "AAA">


    <!-- This query will be used in order to display the available categories in a table -->
    <cfquery name="getCategories"
             datasource="#Request.DSN#"
             username="#Request.username#"
             password="#Request.password#">
      SELECT * FROM tbareacategory
    </cfquery>


    <h4>Categories</h4>

    <!-- Display data in table -->
    <table>
         <tr><th>Category</th></tr>
         <cfoutput query="getCategories">
         <cfform method="post">
         <tr>
          <input type="hidden" name="category" value="#category#">
          <td>#category#</td>
          <td><input type="submit" name="submit" value="Update"></td>
          <td><input type="submit" name="submit" value="Delete"></td>
         </tr>
         </cfform>
        </cfoutput>
    </table>
    <center><cfform><cfinput type="submit" name="submit" value="Add New Category"></cfform></center>




    <!-- if here, provide form to enable update -->
    <cfelseif #Form.submit# EQ "Update">

      <!-- this query will retrieve the category value that will be updated -->
      <cfquery name="updateCategory"
               datasource="#Request.DSN#"
               username="#Request.username#"
               password="#Request.password#">
      SELECT * FROM tbareacategory
      WHERE category = 
      <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.category#">
      </cfquery>


      <h4>Enter new Category name</h4>

    <table>
         <tr><th>Category</th></tr>
         <cfoutput query="updateCategory">
         <cfform action="category_update.cfm" method="post">
         <tr>
          <td>
            <!-- passing values to be used for processing the update -->
            <cfinput type="hidden" name="oldCategory" value="#category#">
            <cfinput name="newCategory" type="text" value="#category#"
              size="30" maxlength="30">
          </td>
          <td><input type="submit" name="submit" value="Update"</td>
         </tr>
         </cfform>
        </cfoutput>
    </table>



    


    <cfelseif #Form.submit# EQ "Delete">


      <cftry> 
            <cfquery name="deleteCategory"
                   datasource="#Request.DSN#"
                   username="#Request.username#"
                   password="#Request.password#">
          DELETE FROM tbareacategory
          WHERE category = 
          <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.Category#">
          </cfquery>
         
          <h4>Delete Successful</h4>
          <center><a href="categories.cfm">Back</a></center>



        <!-- delete failed.. display message -->
      <cfcatch type="database"> 

            <h4>This database entry is parent to entries in other tables. Cannot delete.</h4>
            <center><a href="categories.cfm">Back</a></center>
            <cfabort>

      </cfcatch>
      </cftry>














    <cfelseif #Form.submit# EQ "Add New Category">
      <cfoutput><cflocation url="category_add.cfm"></cfoutput>


    <cfelse>
    <h4>There is a problem with the form</h4>
    <center><a href="categories.cfm">Back</a></center>


    </cfif>
    <cfinclude template = "footer.cfm">


</body>
</html>