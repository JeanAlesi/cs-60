<!DOCTYPE html>
<html>
  <head>
    <title></title>
    <cfinclude template = "./css/style.css">
  </head>
  <body>
  <!-- this page will display the seats available in the database. Seats are instances of offices
     E.g.: President of the U.S. for the term 2016 is a "seat". 
           President is the "office"
           U.S. is the "category" 
           2016 is the "term" -->

    <cfparam name="projectName" default="Project4" type="string">
    <cfinclude template = "header.cfm">
    


    <cfif isdefined("Form.update")>

      <!-- this query will be used to display the current values of the seat to be updated -->
      <cfquery name="getSeats_update"
               datasource="#Request.DSN#"
               username="#Request.username#"
               password="#Request.password#">
               SELECT o.description AS office, a.description as area, s.officeid, s.areaid, term 
               FROM tbarea a, tboffice o, tbseat s
               WHERE a.areaid = s.areaid
               AND o.officeid = s.officeid
               AND s.areaid = 
               <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.areaid#">
               AND s.officeid = 
               <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.officeid#">
               AND term = 
               <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.term#">
      </cfquery>
      

      <table>
         <tr><th>Office</th><th>Area</th><th>Term</th></tr>
         <tr>
          <cfform action="" method="post">
          <cfoutput query="getSeats_update">
          <td>#office#</td>
          <td>#area#</td>
          <td>
            <cfinput name="new_term" type="text" value="#term#" size="2"
               maxlength="2">
          </td>
                    
          <td>
              
              <input type="hidden" name="areaid" value="#areaid#">
              <input type="hidden" name="officeid" value="#officeid#">
              <input type="hidden" name="term" value="#term#">
              <input name="update2" type="submit" value="Ok">
          </td>
          </cfoutput>
          </cfform>
          </tr>
    </table>

    <!-- second state in updating a seat -->
    <cfelseif isdefined("Form.update2")>

      <cfquery  name="checkDatabase"
                datasource="#Request.DSN#"
                username="#Request.username#"
                password="#Request.password#">
      SELECT * FROM tbseat
      WHERE officeid =
      <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.officeid#">
      AND areaid = 
      <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.areaid#">
      AND term = 
      <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.new_term#">
      </cfquery>



   <cfif checkDatabase.RecordCount IS 0>

      <cfquery  name="getAreaCategory"
                datasource="#Request.DSN#"
                username="#Request.username#"
                password="#Request.password#">
      UPDATE tbseat SET term = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.new_term#">
      WHERE areaid = 
      <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.areaid#">
      AND officeid =
      <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.officeid#">
      AND term =
      <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.term#">
      </cfquery>

      <cfoutput>
         <cflocation url="seats.cfm">
      </cfoutput>
   
   <cfelse>
      <center><h4>Data already found in database.</h4><a href="seats.cfm">Back</a>
   </cfif>



   <!-- admin chose to delete a seat -->
   <cfelseif isdefined("Form.delete")>
      <cfquery  name="deleteSeat"
                datasource="#Request.DSN#"
                username="#Request.username#"
                password="#Request.password#">
           DELETE FROM tbseat
           WHERE areaid = 
           <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.areaid#">
           AND officeid =
           <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.officeid#">
           AND term =
           <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.term#">
      </cfquery>

    <cfoutput><cflocation url="seats.cfm"></cfoutput>


    <cfelse> <!-- no forms submitted. Will simply display seats --> 

    <cfquery name="getSeats"
             datasource="#Request.DSN#"
             username="#Request.username#"
             password="#Request.password#">
             SELECT o.description AS office, a.description as area, s.officeid, s.areaid, term 
             FROM tbarea a, tboffice o, tbseat s
             WHERE a.areaid = s.areaid
             AND o.officeid = s.officeid
             ORDER BY 1 DESC
    </cfquery>
    
    <h4>Select:</h4>
    <table>
         <tr><th>Office</th><th>Area</th><th>Term</th></tr>
        <cfoutput query="getSeats">
         <tr>
          <td>#office#</td>
          <td>#area#</td>
          <td>#term#</td>
          <td>
              <cfform action="" method="post">
              <input type="hidden" name="areaid" value="#areaid#">
              <input type="hidden" name="officeid" value="#officeid#">
              <input type="hidden" name="term" value="#term#">
              <input name="update" type="submit" value="Update">
              </cfform>
          </td>
          <td>
              <cfform action="" method="post">
              <input type="hidden" name="areaid" value="#areaid#">
              <input type="hidden" name="officeid" value="#officeid#">
              <input type="hidden" name="term" value="#term#">
              <input name="delete" type="submit" value="Delete">
              </cfform>
          </td>
          </tr>
        </cfoutput>
    </table>



      <center>
          <cfform action="seat_add.cfm" method="post">
          <input name="add" type="submit" value="Add Seat">
          </cfform>
      </center>


    </cfif>


    <cfinclude template = "footer.cfm">
  </body>
</html>
