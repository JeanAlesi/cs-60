<!DOCTYPE html>
<html>
<head>
    <cfinclude template = "./css/style.css">
</head>
<body>
    <cfparam name="projectName" default="Final Project" type="string">
    <cfinclude template = "header.cfm">

<h2>Walkthrough</h2>

<p>This App allows you to create polls via a password protected folder, and allows the public to participate in those polls.. Loging in can be done via the login page. However, you will be automatically prompted to login whenever visiting a password protected page. The username and password are given in the login page.</p>

<p>The index page has links to pages that control add, update, and delete functions for every table in the database. Every page in the app displays the data available in its corresponding table, and provides buttons for the add, update, delete functions.</p>
<img src="./images/walkthrough_1.PNG">



<p>For example, in the candidates page (candidates.cfm) the authorized user will be shown the candidates currently available. Adding, Updating, and Deleting functions are accessible through the cfform submit buttons.</p>


<img src="./images/walkthrough_2.PNG">


<p>One of the most important features of this App is the ability to create polls. Before creating a poll, you will be prompted to choose a Area Category from the tbareacategory table. Some tables may require that you choose values from rows available in other tables. For example: When creating a poll, you may select what candidates to add only among the candidates that have a campaign.</p>


<img src="./images/walkthrough_3.PNG">


<p>dev_polls.cfm is the developer's page for creating, updating, and deleting polls.</p>


<img src="./images/walkthrough_4.PNG">


<p>When creating a poll, it is possible to choose any number of candidates among the candidates that are running a campaign that matches the "seat" primary key that was chosen for the poll. E.g.: If when creating the poll, the seat primary key selected was (a, b, c), only campaigns running for (a, b, c) will be available as a choice to participate in the poll.</p>


<img src="./images/walkthrough_5.PNG">


<p>In order to participate in the poll, users should access the public access page located at: <a href="../poll/polls.cfm">"http://cscie60.dce.harvard.edu/~arecalde/poll/polls.cfm/polls.cfm"</a>. When in the said page, you must choose a poll among the polls available. You cannot create polls from this page. Upon submitting you vote/choice for a candidate, you'll be shown the results.</p>


<img src="./images/walkthrough_6">



    <cfinclude template = "footer.cfm">
</body>
</html>