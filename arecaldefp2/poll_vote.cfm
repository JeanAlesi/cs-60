<!DOCTYPE html>
<html>
<head>
    <cfinclude template = "./css/style.css">
</head>
<body>
    <cfparam name="projectName" default="Project4" type="string">
    <cfinclude template = "header.cfm">


<!-- if a vote was submitted -->
<cfif isdefined("Form.submitVote")>
 
    <!-- check to see if a candidate was voted for -->
    <cfif isdefined("Form.c_id")>
        <!-- insert the vote for the specified candidate -->
        <cfquery  name="pollVote"
                  datasource="#Request.DSN#"
                  username="#Request.username#"
                  password="#Request.password#">
          INSERT into tbpollvote values (seq_pollvoteid.nextval,
          <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.pollid#">,
          <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.c_id#">)
        </cfquery>

        <cfoutput><cflocation url="polls.cfm?results=#Form.pollid#"></cfoutput>
        

    <cfelse> <!-- no candidate selected -->
        you casted a blank vote. Thanks for participating. 
        <cfoutput><a href="polls.cfm?results=#Form.pollid#">Back</a></cfoutput>   
    </cfif>


<cfelse> <!-- if this page was accessed without submitting a vote, redirect to the polls page -->
    <cfoutput><cflocation url="polls.cfm"></cfoutput>


</cfif>

    <cfinclude template = "footer.cfm">
</body>
</html>