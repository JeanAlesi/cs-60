<!DOCTYPE html>
<html>
  <head>
    <title>Final Project</title>
    <cfinclude template = "./css/style.css">
  </head>
  <body>
    <cfparam name="URL.category" default="AAA" type="string">
    <cfparam name="projectName" default="Final Project" type="string">
    <cfinclude template = "header.cfm">

    
    <!-- area category not specfied... return to area.cfm -->
    <cfif #URL.category# EQ "AAA">
      <cfoutput><cflocation url="areas.cfm"></cfoutput>
      

    <!-- if here, an area category has been selected at area.cfm -->
    <cfelse>

      <!-- if true, insert the new data -->
      <cfif isdefined("Form.submit")>

        <!-- further validate the data before insert -->  
        <cfif Trim("#Form.areaDescription#") EQ "">
          <h4>
            Please enter a valid Area Description.
            <cfoutput><a href="area_create.cfm?category=#URL.category#">Back</a></cfoutput>
          </h4>

        <cfelse> <!-- proceeds to insert a new area into tbarea -->

          <cfquery name="getAreas"
                   datasource="#Request.DSN#"
                   username="#Request.username#"
                   password="#Request.password#">
              INSERT into tbarea values
              <!--- 'AAA' here, because the trigger will replace it with a sequenced value---> 
              ('AAA', 
              <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.areaDescription#">,
              <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#URL.category#">)
          </cfquery>

          <!-- after the insert, redirect -->
          <cfoutput><cflocation url="areas.cfm"></cfoutput>

        </cfif> <!-- end cfif for input validation -->

      <!-- if here, proceed to form -->
      <cfelse>

        <h4>Create an Area value</h4>

        <table>
          <cfform>
          <tr>
          <td>Enter Area Description:</td>
          <td>
            <cfinput name="areaDescription" type="text"
              size="30" maxlength="30">
          </td>
          <td><cfinput name="submit" type="submit" value="Create"></td>
          </tr>
          </cfform>
        </table>
       

      </cfif>
    </cfif>
    <br>
    <center><a href="areas.cfm">Back</a></center>
    <cfinclude template = "footer.cfm">
  </body>
</html>