<!DOCTYPE html>
<html>
<head>
    <cfinclude template = "./css/style.css">
</head>
<body>
    <cfparam name="projectName" default="Final Project" type="string">
    <cfinclude template = "header.cfm">


<ul>
<li><a href="login.cfm">Login</a></li>
<li><a href="logout.cfm">Logout</a></li>
<li><a href="dev_polls.cfm">dev_polls.cfm</a></li>
<li><a href="../poll/polls.cfm">polls.cfm (public access)</a></li>
<li><a href="campaigns.cfm">campaigns.cfm</a></li>
<li><a href="candidates.cfm">candidates.cfm</a></li>
<li><a href="seats.cfm">seats.cfm</a></li>
<li><a href="offices.cfm">offices.cfm</a></li>
<li><a href="areas.cfm">areas.cfm</a></li>
<li><a href="categories.cfm">categories.cfm</a></li>
<br>
<li><button><a href="datamodel.pdf">Data Model & Record Diagrams</a></button></li>
<br>
<li><button><a href="walkthrough.cfm">Walkthrough</a></li></button>
</ul>





    <cfinclude template = "footer.cfm">
</body>
</html>