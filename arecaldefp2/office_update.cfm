<!DOCTYPE html>
<html>
<head>
    <cfinclude template = "./css/style.css">
</head>
<body>
    <cfparam name="projectName" default="Final Project" type="string">
    <cfinclude template = "header.cfm">





    	<!-- Ensure that the Form has been submitted properly -->
    <cfif isDefined("Form.newDescription") AND isDefined("Form.oldDescription")>
      <cfif Trim("Form.newDescription") EQ "">
        <h4>Please enter a valid office name. <a href="officess.cfm">Back</a></h4>
      <cfelse>

        <cftry> 


          <cfquery name="processUpdateCategory"
                   datasource="#Request.DSN#"
                   username="#Request.username#"
                   password="#Request.password#">
          UPDATE tboffice
          SET description = 
          <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.newDescription#">
          WHERE officeid = 
          <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.officeid#">
          </cfquery>
          <h4>Update Successful</h4>


        <!-- catch if update failed.. display message -->
        <cfcatch type="database"> 

            <h4>This database entry is parent to entries in other tables, or there is another entry with the same name.</h4>
            <center><a href="offices.cfm">Back</a></center>
            <cfabort>

        </cfcatch>
        </cftry>

      </cfif> <!-- Trim string -->

    <cfelse>
      <h4>Sorry, there is a problem with the Form</h4>
        

    </cfif>

      <center><a href="offices.cfm">Back</a></center>




    <cfinclude template = "footer.cfm">
</body>
</html>