<!DOCTYPE html>
<html>
<head>
    <cfinclude template = "./css/style.css">
</head>


<!-- enable deactivation of polls... closed polls -->

<body>
    <cfparam name="projectName" default="Project4" type="string">
    <cfparam name="pollid" default="AAA" type="string">
    <cfinclude template = "header.cfm">


<cfif isdefined("Form.deletePoll")>
      <cfquery name="deletePoll"
           datasource="#Request.DSN#"
           username="#Request.username#"
           password="#Request.password#">
           DELETE FROM tbpoll
           WHERE pollid =
           <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#Form.pollid#">
      </cfquery>
      <cfoutput><cflocation url="dev_polls.cfm"></cfoutput>





<cfelse>
    <!-- display the currently available polls -->
    <cfquery name="getPoll"
           datasource="#Request.DSN#"
           username="#Request.username#"
           password="#Request.password#">
           SELECT pollid, pollname, p.areaid, a.description as area,
           p.officeid, o.description as office, term
           FROM tbpoll p, tboffice o, tbarea a
           WHERE  p.areaid = a.areaid
           AND    p.officeid = o.officeid
           ORDER BY 2
    </cfquery>


    <h4>Select:</h4>
    <table>
         <tr><th>ID </th><th>Poll name</th><th>Area</th><th>Office</th><th>Term</th></tr>
        <cfoutput query="getPoll">
         <tr>
          <td>#pollid#</td>
          <td>#pollname#</td>
          <td>#office#</td>
          <td>#area#</td>
          <td>#term#</td>
          <td><a href="poll_update.cfm?pollid=#pollid#"><button>Update</button></a></td>
          <td>
              <cfform method="post">
              <input type="hidden" name="pollid" value="#pollid#">
              <input type="submit" name="deletePoll" value="Delete">
              </cfform>
          </td>
          </tr>
        </cfoutput>
    </table>
      <!-- links to the poll_create page -->
      <center><a href="poll_create.cfm">Create a new poll</a></center>


</cfif>

    <cfinclude template = "footer.cfm">
</body>
</html>