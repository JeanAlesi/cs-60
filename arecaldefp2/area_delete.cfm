<!DOCTYPE html>
<html>
  <head>
    <title>Final Project</title>
    <cfinclude template = "./css/style.css">
  </head>
  <body>
  	<cfparam name="projectName" default="Final Project" type="string">
    <cfinclude template = "header.cfm">


		<cftry> <!-- try to delete -->

		  	<cfquery name="deleteArea"
		               datasource="#Request.DSN#"
		               username="#Request.username#"
		               password="#Request.password#">
		              DELETE FROM tbarea
		              WHERE areaid =
		              <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.areaid#">
			</cfquery>
			<cfoutput><cflocation url="areas.cfm"></cfoutput>

		<!-- delete failed.. display message -->
		<cfcatch type="database"> 

		    <h4>This database entry is parent to entries in other tables. Cannot delete.</h4>
		    <center><a href="areas.cfm">Back</a></center>
		    <cfabort>

		</cfcatch>
		</cftry>





    <cfinclude template = "footer.cfm">
  </body>
</html>