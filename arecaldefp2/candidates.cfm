<!DOCTYPE html>
<html>
  <head>
    <title>Final Project</title>
    <cfinclude template = "./css/style.css">
  </head>
  <body>
    <cfparam name="Form.candidateid" default="AAA" type="string">
    <cfparam name="projectName" default="Final Project" type="string">
    <cfinclude template = "header.cfm">
    

    <!-- if true, the user clicked the update button for a given candidate -->
    <cfif isdefined("Form.update")>
          
          <!-- this will display what values the candidate currently has in tbcandidate -->
          <cfquery name="fetchCandidate"
                   datasource="#Request.DSN#"
                   username="#Request.username#"
                   password="#Request.password#">
              SELECT * FROM tbcandidate 
              WHERE candidateid = 
              <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.candidateid#">
          </cfquery>

          <cfform method="post">
          <table>
            <tr>
            <th>ID</th>
            <th>Name</th>
            </tr>

            <tr>
            <cfoutput>
            <td>#fetchCandidate.candidateid#</td>
            <td><input name="candidatename" type="text" value="#fetchCandidate.candidatename#"
              size="30" maxlength="30"></td>
            <td>
            <input type="hidden" name="candidateid" value="#Form.candidateid#">
            <input name="update2" type="submit" value="Update"> <!-- update with the new values -->
            </td>
            </cfoutput>
            </tr>
          </table>
          </cfform>


    <!-- if true, the user selected new values for a given candidate. Proceeds to update in database -->
    <cfelseif isdefined("Form.update2")>
          <cfquery  name="updateCandidate"
                    datasource="#Request.DSN#"
                    username="#Request.username#"
                    password="#Request.password#"
                    result="updateResult">
              UPDATE tbcandidate
              SET candidatename = 
              <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.candidatename#">
              WHERE candidateid =
              <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.candidateid#">
          </cfquery>
          <cfoutput><cflocation url="candidates.cfm"></cfoutput>
        

    <cfelseif isdefined("Form.add")>

          <cfform method="post">
          <table>
            <tr><th>Enter Candidate Name</th></tr>
            <tr>
              <td>
              <input name="candidatename" type="text">
              </td>
              <td>
              <input name="add2" type="submit" value="Add Candidate"> 
              </td>
            </tr>
          </table>
          </cfform>


    <cfelseif isdefined("Form.add2")>
      <!-- trimming to avoid empty or "   " values or the alike -->
      <cfif Trim("#Form.candidatename#") EQ "">
        <h4>Please enter a valid name. <a href="candidates.cfm">Back</a></h4>
      <cfelse>
        <!-- proceeds to insert a new candidate into the tbcandidate table -->
        <cfquery  name="addCandidate"
                  datasource="#Request.DSN#"
                  username="#Request.username#"
                  password="#Request.password#"
                  result="updateResult">
              INSERT into tbcandidate values (seq_candidateid.nextval, 
              <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.candidatename#">)
          </cfquery>
          <cfoutput><cflocation url="candidates.cfm"></cfoutput>
      </cfif>


    <cfelseif isdefined("Form.delete")>

    <!-- display candidates that can be deleted -->
    <cfquery name="getCandidates_delete"
             datasource="#Request.DSN#"
             username="#Request.username#"
             password="#Request.password#">
      select candidateid, candidatename from tbcandidate
    </cfquery>

    <div class="wrapper">
      <form action="" method="post">
      <table>
      <tr>
      <th>Candidate: </th>
      <td>
      <select name="candidateid">
      <cfoutput query="getCandidates_delete">
      <option value="#candidateid#">#candidatename#</option>
      </cfoutput>
      </select>
      </td>
      </tr>
      <tr>
      <td>&nbsp;</td>
      <td>
      <input name="delete2" type="submit" Value="Delete"/>
      </td>
      </tr>
      </table>
      </form>
    </div>

    <cfelseif isdefined("Form.delete2")>
      <cfquery name="getCandidates_delete"
             datasource="#Request.DSN#"
             username="#Request.username#"
             password="#Request.password#">
      DELETE FROM tbcandidate
      WHERE candidateid = 
      <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.candidateid#">
    </cfquery>
    <cfoutput><cflocation url="candidates.cfm"></cfoutput>



    <!-- if we just got to the page, we should be here... No forms have been submitted yet -->
    <cfelse>


    <cfquery name="getCandidates"
             datasource="#Request.DSN#"
             username="#Request.username#"
             password="#Request.password#">
      select * from tbcandidate
    </cfquery>

    <!-- next, display a list of candidates -->
    <h4>Candidates</h4>
    <table>
         <tr><th>ID</th><th>Name</th></tr>
        <cfoutput query="getCandidates">
         <tr>
          <td>#candidateid#</td>
          <td>
            <a href="campaigns.cfm?candidateid=#candidateid#" title="See campaigns run by this candidate">#candidatename#</a>
          </td>
          <td>
            <cfform action="" method="post">
            <input type="hidden" name="candidateid" value="#candidateid#">
            <input name="update" type="submit" value="Update">
            </cfform>
          </td></tr>
        </cfoutput>
    </table>

    <cfform action="" method="post">
      <center>
        <input name="add" type="submit" value="Add Candidate">
        <input name="delete" type="submit" value="Delete Candidate">
      </center>
    </cfform>


    </cfif>


    <cfinclude template = "footer.cfm">
  </body>
</html>