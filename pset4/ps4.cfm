<!DOCTYPE html>
<html>
<head>
	<title>PSET4 Index</title>
    <cfinclude template = "./ps4/style.css">
</head>

<body>
	<h1>Project 6 - Index Page</h1>
	<ul>
	  <li><a href="./ps4/arecaldeps4_project1.lst">Project1</a></li>
	  <li><a href="./ps4/selectproduct.cfm">Project2</a></li>
	  <li><a href="./ps4/storyboard.cfm">Project3</a></li>
	  <li><a href="./ps4/selectcustomer.cfm">Project4 & 5</a></li>
	</ul>
</body>
</html>