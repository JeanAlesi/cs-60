-- ******************************************************
-- arecaldeps4.sql
--
-- Project 1, PSET 4
--
-- Description: This script defines a statement level 
--              trigger and provides subsequent testing
--              in accordance to Project 1 from PSET 4.
--
-- Author:  Antonio Recalde Russo
--
-- Date:   November, 2014
--
-- ******************************************************

SET echo ON;
SET serveroutput ON;


-- ******************************************************
--    SPOOL SESSION
-- ******************************************************


-- Drop triggers before redifining (in case it is set for anther table)
   DROP trigger test;


-- ******************************************************
--    STATEMENT LEVEL TRIGGER
--    PROJECT 1, PSET 4
-- ******************************************************

   CREATE OR REPLACE TRIGGER test
   BEFORE INSERT ON tbseat
   declare
      x varchar2(10);
      y varchar2(10);
   begin
      SELECT category into x 
      FROM tboffice
      WHERE officeid = new:officeid;

      SELECT category into y
      FROM tbarea
      WHERE areaid = new:areaid;


      if x <> y then
         dbms_output.put_line (
            'Not equal!');
      end if;
   end test;
/  






INSERT into tbseat values ('005', '000', '14');
