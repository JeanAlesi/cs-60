-- ******************************************************
-- arecaldefp.sql
--
-- Database for FINAL PROJECT
--
-- Author:  Antonio Recalde Russo
--
-- Last Modified:   December, 2014
--
-- ******************************************************

-- ******************************************************
--		SPOOL SESSION
-- ******************************************************

spool fp.lst


-- ******************************************************
--		DROP TABLES
-- ******************************************************




-- ******************************************************
-- 		DROP TRIGGERS
-- ******************************************************


-- ******************************************************
--		DROP SEQUENCES
-- ******************************************************


-- ******************************************************
--		CREATE TABLES
-- ******************************************************

CREATE table tbcandidate (
	candidateid			char(5)			not null
		constraint rg_candidateid check (candidateid between '00000' and '99999')
		constraint pk_tbcandidate primary key,
	candidatename		varchar2(30)	not null,
	dateofbirth			date 			not null
);


CREATE table tboffice (
	officeid			char(3)			not null
		constraint rg_officeid check (officeid between '000' and '999')
		constraint pk_officeid primary key,
	description			varchar2(30)	not null
);

CREATE table tbissue (
	issueid				char(3)			not null
		constraint rg_issueid check (issueid between '000' and '999')
		constraint pk_issueid primary key;
);

CREATE table tbarea (
	areaid				char(5)			not null
		constraint rg_areaid check (areaid between '00000' and '99999')
		constraint pk_areaid primary key,
	description			varchar2(32)	not null
);

CREATE table tbdemographic_category (
	categoryid			char(2)			not null
		constraint rg_categoryid check (categoryid between '00' and '99')
		constraint pk_categoryid primary key,
	description			varchar2(16)	not null
);

CREATE table tbdemographic_group (
	demographicgroupid	char(2)			not null
		constraint rg_demographicgroupid check (demographicgroupid between '00' and '99')
		constraint pk_demographicgroupid primary key,
	description			varchar2(16)	not null
	categoryid  		char(2)			not null
		constraint fk_categoryid_tbdemogroup references tbdemographic_category (categoryid) on delete cascade
		);

CREATE table tbpolitical_stance (
	candidateid  		char(5)			not null
		constraint fk_candidateid_tbpol_sta references tbcandidate (candidateid),
	issueid 			char(3)			not null
		constraint fk_issueid_tbpol_sta	references tbissue (issueid),
	favor				char(1)			not null,
	-- remember to choose between Y/N or 1,2,3,4,5	
		constraint pk_tbpolitical_stance primary key (candidateid, issueid)
);

CREATE table tbseat (
	areaid  			char(5)			not null
		constraint fk_areaid_tbseat references tbarea (areaid),
	officeid 			char(3)			not null
		constraint fk_officeid references tboffice (officeid),
	term 				char(5)			not null,
		constraint pk_tbseat primary key (areaid, officeid, term)
);

CREATE table tbcampaign (
	campaignid			char (5)		not null
		constraint rg_campaignid check (campaignid between '00000' and '99999')
		constraint pk_tbcampaign primary key,
	candidateid			char(5)			not null
		constraint fk_tbcampaign_tbcandidate references tbcandidate (candidateid),
	areaid				char(5)			not null
		constraint fk_tbcampaign_tbarea references tbarea (areaid),
	officeid			char(3)			not null
		constraint fk_tbcampaign_tboffice references tboffice (officeid),
	term				char(5)			not null
	-- remember to make this combination unique through trigger or another
);

CREATE table tbpoll (
	pollid				char(4)			not null
		constraint rg_pollid check (pollid between '0000' and '9999')
		constraint pk_tbpoll primary key,
	areaid  			char(5)			not null,
	officeid 			char(3)			not null,
	term 				char(5)			not null,
		constraint fk_tbpoll_tbseat foreign key (areaid, officeid, term)
		references tbseat (areaid, officeid, term)				
);

CREATE table tbpoll_instance (
	poll_instanceid		char(5)			not null
		constraint rg_poll_instanceid check (poll_instanceid between '00000' and '99999')
		constraint pk_poll_instanceid primary key,
	pollid		char(5)			not null,
	campaignid			char(5)			not null
		constraint fk_tbpoll_inst_tbcampaign references tbcampaign (campaignid)
);

CREATE table tbpoller_political_stance (
	poll_instanceid  			char(5)			not null
		constraint fk_tbpps_tbpoll_ins references tbpoll_instance (poll_instanceid),
	ppsno						char(2)			not null
		constraint rg_ppsno check (pps between '00' and '99'),
	issueid 			char(3)			not null
		constraint fk_tbpoll_tbissue references tbissue (issueid),
	favor				char(1)			not null
	--remember to determine if y/n or 12345
		constraint pk_tbpps primary key (poll_instanceid, ppsno)
);

CREATE table tbpoller_demographic (
	poll_instanceid 			char(5)			not null
		constraint fk_tbpoller_demo_tbpoll references tbpoll_instance (poll_instanceid),
	poller_demographicno		char(2)			not null
		constraint rg_poller_demographicno check (poller_demographicno between '00' and '99'),
	demographicgroupid 			char(2)			not null
		constraint fk_tbpd_tbdg references tbdemographic_group (demographicgroupid),
		constraint pk_tbpoller_demographic primary key (poll_instanceid, poller_demographicno)
);

CREATE table tbdemographics_per_area (
	demographicgroupid	char(2)			not null
		constraint fk_tbdpa_tbdg references tbdemographic_group (demographicgroupid),
	areaid				char(5)			not null
		constraint fk_tbdpg_tbarea references tbarea (areaid),
	quantity			number(10,0)	default 1 	not null,
		constraint pk_dpa primary key (demographicgroupid, areaid)
);