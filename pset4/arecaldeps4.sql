-- ******************************************************
-- arecaldeps4.sql
--
-- Project 1, PSET 4
--
-- Description: This script defines a statement level 
--              trigger and provides subsequent testing
--              in accordance to Project 1 from PSET 4.
--
-- Author:  Antonio Recalde Russo
--
-- Date:   November, 2014
--
-- ******************************************************

SET echo ON;
SET serveroutput ON;
@arecaldeps3;

-- ******************************************************
--    SPOOL SESSION
-- ******************************************************

spool arecaldeps4_project1.lst

-- Drop triggers before redifining (in case it is set for anther table)
   DROP trigger three_quotes_rule;


-- ******************************************************
--    STATEMENT LEVEL TRIGGER
--    PROJECT 1, PSET 4
-- ******************************************************

   CREATE OR REPLACE trigger three_quotes_rule
   AFTER INSERT OR UPDATE OR DELETE ON tbitem
   declare
      x number;
   begin
      SELECT count(*) into x
      FROM
         (SELECT a.productid FROM 
          tbproduct a
          LEFT JOIN
          tbitem b
          ON a.productid = b.productid
          GROUP BY a.productid 
          HAVING COUNT(vendorid) < 3);

      if x > 0 then
         dbms_output.put_line (
            '******* Warning******* Products must be quoted by at least three vendors - THREE_QUOTES_RULE TRIGGER');
      end if;
   end THREE_QUOTES_RULE;
/  




-- ******************************************************
-- TESTING
-- ******************************************************

-- DISPLAYING items currently breaking the three quotes rule

SELECT a.productid, count(DISTINCT vendorid)
FROM tbproduct a
LEFT JOIN
tbitem b
ON a.productid = b.productid
GROUP BY a.productid
HAVING count(vendorid) < 3;

-- DISPLAY THE EXITING ORDERS AND ITEMS
SELECT * FROM tbitem order by productid, vendorid;

-- TEST UPDATE
UPDATE tbitem
SET itemprice = (itemprice + 1)
WHERE productid = 100 AND vendorid = 5100;


-- TEST INSERT
INSERT into tbitem values ('121', '5200', 33, 15);

/* this next insert should not give a warning... Granted nothing has been added to
 the db after running the script from pset3, by this point all products would
 have 3 quotes */
INSERT into tbitem values ('434', '5300', 33, 35);


-- TEST UPDATE (should give no warning)
UPDATE tborderitem
SET itemprice = (itemprice + 1)
WHERE orderno = 1 AND orderitemno = 1;


-- CHECK NUMBER OF DISTINCT QUOTES FOR EACH PRODUCT

SELECT productid, count(distinct vendorid)
FROM tbitem
GROUP BY productid;


-- TEST DELETE (warnings should start)
DELETE FROM tbitem WHERE itemprice = 33;

-- TEST UPDATE
UPDATE tbitem
SET itemprice = (itemprice + 1)
WHERE productid = 100 AND vendorid = 5100;

-- ******************************************************
-- END SESSION
-- ******************************************************
spool off