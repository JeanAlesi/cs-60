<!DOCTYPE html>
<html>
  <head>
    <title>PSET4 - Project 4</title>
    <cfinclude template = "style.css">
  </head>
  <body>

    <cfparam name="projectName" default="Project4" type="string">
    <cfinclude template = "header.cfm">
    <cfquery name="getCustomers"
             datasource="#Request.DSN#"
             username="#Request.username#"
             password="#Request.password#">
      select customerid, customername
         from tbcustomer
         order by 2
    </cfquery>
    
    <h4>Select a Customer</h4>
    <table>
         <tr><th>ID</th><th>Name</th></tr>
        <cfoutput query="getCustomers">
         <tr>
          <td>#getCustomers.customerid#</td>
          <td>
            <a href="showorderitem.cfm?customerid=#getCustomers.customerid#">#getCustomers.customername#</a></td>
         </tr>
        </cfoutput>
    </table>











    <cfinclude template = "footer.cfm">
  </body>
</html>
