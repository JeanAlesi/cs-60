<!DOCTYPE html>
<html>
  <head>
    <title>Show Orders & Items</title>
    <cfinclude template = "style.css">
  </head>
  <body>
    <cfparam name="projectName" default="Project4" type="string">
    <cfparam name="URL.customerid" default="AAA" type="string">

    <cfinclude template = "header.cfm">
    

    <cfif URL.customerid NEQ "AAA">
    <cfquery  name="getCustomer"
              datasource="#Request.DSN#"
              username="#Request.username#"
              password="#Request.password#">
       SELECT customerid FROM tbcustomer
       WHERE customerid = 
          <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#URL.customerid#">
    </cfquery>


    <cfquery  name="getOrderItems"
              datasource="#Request.DSN#"
              username="#Request.username#"
              password="#Request.password#">
       SELECT a.orderno, b.orderitemno, productname, quantity, itemprice FROM 
       tborder a
       INNER JOIN
       tborderitem b
       ON a.orderno = b.orderno
       INNER JOIN
       tbproduct c
       ON b.productid = c.productid
       WHERE a.customerid = 
          <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#URL.customerid#">
    </cfquery>

    <cfif getCustomer.RecordCount IS 0>
      Customer not in database. <br>
      Please <a href="selectcustomer.cfm">try again</a>

    <cfelse>
      <h3>Results for customer: <cfoutput>#getCustomer.customerid#
      </cfoutput></h3>

      <h4>
        <cfif getOrderItems.RecordCount IS 0>
         Currently there are no orders by this customer
        <cfelseif getOrderItems.RecordCount IS 1>
         There is 1 ordered item by this customer for this project
        <cfelse>
         There are
         <cfoutput>#getOrderItems.RecordCount#</cfoutput>
         ordered items by this customer
        </cfif>  
      </h4>
    </cfif>



    <cfif getOrderItems.RecordCount GT 0>
      

       <table>
           <tr>
             <th>Order#</th>
             <th>Product</th>
             <th>Quantity</th>
             <th>Item Price</th>

           </tr>
         <cfoutput query="getOrderItems">
         <cfform action="updateorderitem.cfm" method="post">
           <tr>
             <td>#orderno#</td>
             <td>#productname#</td>
             <td>#quantity#</td>
             <td>#itemprice#</td>
             <td>
                <input type="hidden" name="orderitemno" value="#orderitemno#">
                <input type="hidden" name="orderno" value="#orderno#">
                <input type="hidden" name="customerid" value="#customerid#">
                <input type="submit" value="Update">
             </td>
           </tr>
         </cfform>
         </cfoutput>
       </table>
    </cfif>



    <cfelse>




    <cfquery name="getCustomers"
             datasource="#Request.DSN#"
             username="#Request.username#"
             password="#Request.password#">
      select customerid, customername
         from tbcustomer
         order by 2
    </cfquery>
    <h3>Please select a Customer</h3>
    <table>
         <tr><th>ID</th><th>Name</th></tr>
        <cfoutput query="getCustomers">
         <tr>
          <td>#getCustomers.customerid#</td>
          <td>
            <a href="showorderitem.cfm?customerid=#getCustomers.customerid#">#getCustomers.customername#</a></td>
         </tr>
        </cfoutput>
    </table>

    </cfif> 
    <h3><a href="selectcustomer.cfm">Back</a></h3>
    <cfinclude template = "footer.cfm">



  </body>
</html>
