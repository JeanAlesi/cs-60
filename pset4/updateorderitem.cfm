<!DOCTYPE html>
<html>
<head>
	
</head>
<body>
	
	<cfif isdefined("Form.update")>
		<cfquery  name="updateOrderItem"
              	  datasource="#Request.DSN#"
              	  username="#Request.username#"
                  password="#Request.password#"
                  result="updateResult">
       	UPDATE tborderitem
       	SET quantity = 
       		<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.quantity#">,
       	    itemprice = 
       		<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.itemprice#">
       	WHERE orderno = 
       		<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.orderno#">
       	AND orderitemno = 
       		<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.orderitemno#">
    	</cfquery>
	
		<cfoutput>
	    	<cflocation url="showorderitem.cfm?customerid=#Form.customerid#">
	    </cfoutput>
	
	<cfelse>


	<cfquery  name="fetchOrderItem"
              datasource="#Request.DSN#"
              username="#Request.username#"
              password="#Request.password#">
       	SELECT orderno, orderitemno, productname, quantity, itemprice 
       	FROM tborderitem a, tbproduct b
		WHERE a.productid = b.productid
		AND a.orderno = 
          	<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.orderno#">
		AND a.orderitemno = 
			<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Form.orderitemno#">
    </cfquery>



  <cfform method="post">
    <table>
    <tr>
      <th>Product</th>
      <th>Quantity</th>
      <th>Item Price</th>
  </tr>

  <tr>
  	<cfoutput>
    <td>#fetchOrderItem.productname#</td>
    <td><input name="quantity" type="number" value="#fetchOrderItem.quantity#"
    	size="3" maxlength="3"></td>
    <td><input name="itemprice" type="number"
           value="#fetchOrderItem.itemprice#" size="5" maxlength="5"></td>
    <td>
    	<input type="hidden" name="orderno" value="#Form.orderno#">
        <input type="hidden" name="orderitemno" value="#Form.orderitemno#">
        <input type="hidden" name="customerid" value="#Form.customerid#">

        <input name="update" type="submit" value="Update OrderItem">
    </td>
    </cfoutput>
  </tr>

   </table>
</cfform>
	
</cfif>

<cfinclude template = "footer.cfm">



</body>
</html>