<html>
<head>
   <title>Show Quotes</title>
   <cfinclude template = "style.css">

</head>
<body>


<cfif NOT isdefined("Form.productid") 
   OR Form.productid eq "error" >
   <cflocation url="selectproduct.cfm">
</cfif>


<cfquery name="getProduct"
         datasource="#Request.DSN#"
         username="#Request.username#"
         password="#Request.password#">
   SELECT productid, productname
   FROM tbproduct
   WHERE productid = 
<cfqueryparam cfsqltype="CF_SQL_VARCHAR"
   value=#Form.productid#>
</cfquery>

<cfquery name="getQuotes"
datasource="#Request.DSN#"
username="#Request.username#"
password="#Request.password#">

SELECT distinct productid, vendorid, itemprice 
FROM tborderitem 
WHERE productid =
   <cfqueryparam cfsqltype="CF_SQL_VARCHAR"
      value=#Form.productid#>
order by 1
</cfquery>

<div class="wrapper">
<cfoutput>
   
   <cfif getProduct.RecordCount IS 0>
      Invalid Product
   <cfelse>   

      <h1>Product: #getProduct.productname#<br></h1>
 
      <cfif getQuotes.RecordCount IS 0>
         Currently there are no quotes for this product
      <cfelseif getQuotes.RecordCount IS 1>
         There is 1 quote for this product
      <cfelse>
         There are #getQuotes.RecordCount# quotes for this product<br>
      </cfif> 
   
   </cfif>
</cfoutput>


<cfif getQuotes.RecordCount GT 0>

<table>
<tr>
<th>Product</th>
<th>Vendor</th>
<th>Price Quote</th>
</tr>
<cfoutput query="getQuotes">
<tr>
<td style="text-align: center">#productid#</td>
<td>#vendorid#</td>
<td>#itemprice#</td>
</tr>
</cfoutput>
</table>
</div>

</cfif>

<h3><a href="selectproduct.cfm">Back</a></h3>


</body>
</html>